using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] GameObject explosionVFX;
    
    private void OnTriggerEnter(Collider other)
    {

        StartCrashSequence();
    }

    void StartCrashSequence()
    {
        GetComponent<PlayerControls>().enabled = false;
        explosionVFX.GetComponent<ParticleSystem>().Play();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        Invoke("RestartLevel", 1f);
    }
    private void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}
