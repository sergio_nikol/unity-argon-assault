using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject deathVSFX;
    [SerializeField] GameObject hitVFX;
    [SerializeField] int scoreToIncrease=10;
    [SerializeField] int hp=3;
    ScoreBoard scoreBoard;
    GameObject parent;

    private void Start()
    {
        scoreBoard = FindObjectOfType<ScoreBoard>();
        parent = GameObject.FindWithTag("SpawnAtRuntime");
        AddRigidbody();
    }

    void AddRigidbody()
    {
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();
        KillEnemy();
    }

    void ProcessHit()
    {
        hp--;
        GetComponent<MeshRenderer>().material.color = Color.red;
        Invoke("BringBackColor", 0.2f);

        GameObject vfx = Instantiate(hitVFX, transform.position, Quaternion.identity);
        vfx.transform.parent = parent.transform;
    }
    void KillEnemy()
    {
        if (hp <= 0) {
            scoreBoard.IncreaseScore(scoreToIncrease);
            GameObject vfx = Instantiate(deathVSFX, transform.position, Quaternion.identity);
            vfx.transform.parent = parent.transform;
            Destroy(gameObject);
        }
    }
    void BringBackColor()
    {
        GetComponent<MeshRenderer>().material.color = Color.white;
    }
}
